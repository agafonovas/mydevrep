from django.conf.urls import url
from . import views

urlpatterns = [
    # index
    url(r'^$', views.IndexView.as_view(), name='post_list'),
    # post detail
    url(r'^post/(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='post_detail'),
    # create new post
    url(r'^post/new/$', views.PostCreate.as_view(), name='post_form'),
    # edit post
    url(r'^post/(?P<pk>[0-9]+)/edit/$', views.PostEditView.as_view(), name='post_edit'),
    # delete post
    url(r'^post/(?P<pk>[0-9]+)/delete/$', views.PostDelete.as_view(), name='post_delete'),
]
