from django.shortcuts import render
from .models import Post
from django.utils import timezone
from django.shortcuts import render, get_object_or_404
from .forms import PostForm
from django.shortcuts import redirect
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy
from django.core.paginator import Paginator


class IndexView(generic.ListView):
    template_name = 'post_list.html'
    context_object_name = 'posts'
    paginate_by = 2
    queryset = Post.objects.all()
    # def get_queryset(self):
    #     return Post.objects.all()

class DetailView(generic.DetailView):
    model = Post
    template_name = 'blog/post_detail.html'
    context_object_name = 'post'


class PostCreate(CreateView):
    model = Post
    fields = ['title', 'text', 'author']
    success_url = reverse_lazy('post_list')


class PostEditView(UpdateView):
    model = Post
    fields = ['title', 'text', 'author']

class PostDelete(DeleteView):
    model = Post
    success_url = reverse_lazy('post_list')
